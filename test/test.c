// test.c
//------------------------------------------------------------------------------ 
// Esta es una aplicación de prueba, es una pequeña implemantación de las listas
// enlazadas aplicado al manejo de una lista de contactos.
//------------------------------------------------------------------------------ 

// librerías
//------------------------------------------------------------------------------ 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


// estructuras
//------------------------------------------------------------------------------ 
typedef struct nodo {
    char nombre[20];
    char telefono[15];
    struct nodo *siguiente;
} nodo;

// prototipos
//------------------------------------------------------------------------------ 
void no_saltos(char *cadena);
nodo *crear_contacto();

void agregar_contacto(nodo **lista, nodo **contacto);
void imprimir_lista(nodo *lista);

void validar_memoria(nodo **memoria);
void validar_archivo(FILE **archivo);
void validar_lista(nodo **lista);

void descargar_lista(nodo **lista, FILE **archivo);
void cargar_lista(nodo **lista, FILE **archivo);

// funcion principal
//------------------------------------------------------------------------------ 
int main() {
    // variables 
    nodo *lista_contactos = NULL;
    nodo *nuevo_contacto = NULL;
    FILE *archivo_lista;

    int i = 0;
    
    cargar_lista(&lista_contactos, &archivo_lista);

    // ciclo principal
    while (i < 5) {
        nuevo_contacto = crear_contacto();
        agregar_contacto(&lista_contactos, &nuevo_contacto);
        i++;
    }
    
    // salida de datos
    descargar_lista(&lista_contactos, &archivo_lista);

    imprimir_lista(lista_contactos);
    

    // fin del programa
    return 0;
}


// funciones para manejo de la lista
//------------------------------------------------------------------------------ 
void agregar_contacto(nodo **lista, nodo **contacto) {
    // valída si la lista está vacía.
    if (*lista == NULL) *lista = *contacto;
    else {
        // se necesita utilizar un nodo auxiliar para que la lista no se sobre
        // escriba asi misma con solo los ultimos dos valores.
        nodo *auxiliar = *lista;
        // si hay más valores, entonces adelanta el apuntador en el ciclo anterior 
        // hasta la ultima posición para que el nuevo valor sea agregado en el
        // siguiente de la ultima posición encontrada.
        while (auxiliar->siguiente) {
            auxiliar = auxiliar->siguiente;
        }
        // si solo hay un valor en la lista, el segundo valor se agrega en siguiente.
        auxiliar->siguiente = *contacto;
    }
}

//------------------------------------------------------------------------------ 
nodo *crear_contacto() {
    // reserva espacio en memoria para un nuevo nodo. 
    nodo *contacto = (nodo *) malloc(sizeof(nodo));

    // valída si se reservó la memoria correctamente.
    validar_memoria(&contacto);
    
    // pedir datos al usuario
    printf("Nombre: ");
    fgets(contacto->nombre, 20, stdin);
    no_saltos(contacto->nombre);         
    printf("Telefono: ");
    fgets(contacto->telefono, 15, stdin);
    no_saltos(contacto->telefono);
    contacto->siguiente = NULL;

    // devuelve el apuntador al contacto
    return contacto;
}

//------------------------------------------------------------------------------ 
void imprimir_lista(nodo *lista) {
    // valída si la lista está vacía
    if (!lista) puts("¡La lista está vacía!");
    else {
        puts("============================================================");
        while (lista) {
            printf("Contacto: %-15s\t", lista->nombre);
            printf("Telefono: %s\n", lista->telefono);
            lista = lista->siguiente;
        }
    }
}

//------------------------------------------------------------------------------ 
void no_saltos(char *cadena) {
    cadena[strlen(cadena)-1] = '\0';
}

//------------------------------------------------------------------------------ 
void validar_memoria(nodo **memoria) {
    if (*memoria == NULL) {
        puts("¡Error al reservar la memoria!");
        exit(1);
    }
}

//------------------------------------------------------------------------------ 
void validar_archivo(FILE **archivo) {
    if (*archivo == NULL) {
        puts("¡Error al abriendo el archivo!");
        exit(1);
    }
}

void validar_lista(nodo **lista) {
    if (*lista == NULL) {
        puts("¡Error, la lista está vacía!");
        exit(1);
    }
}

//------------------------------------------------------------------------------ 
void descargar_lista(nodo **lista, FILE **archivo) {
    validar_lista(lista);
    char *nombre_archivo = "lista_contactos.txt";
    char *modo_apertura = "ab";
    *archivo = fopen(nombre_archivo, modo_apertura);
    validar_archivo(archivo);
    // procedimiento
    while (*lista) {
        fwrite((*lista)->nombre, sizeof((*lista)->nombre), 1, *archivo);
        fwrite((*lista)->telefono, sizeof((*lista)->telefono), 1, *archivo);
        *lista = (*lista)->siguiente;
    }
    fclose(*archivo);
}

//------------------------------------------------------------------------------ 
void cargar_lista(nodo **lista, FILE **archivo) {
    // variables
    char *nombre_archivo = "lista_contactos.txt";
    char *modo_apertura = "rb";
    // inicialización del archivo
    *archivo = fopen(nombre_archivo, modo_apertura);
    validar_archivo(archivo);
    // contacto
    nodo *contacto; 
    // bucle
    while (!feof(*archivo)) {
        contacto = (nodo *) malloc(sizeof(nodo));
        validar_memoria(&contacto);
        fread(contacto->nombre, sizeof((*lista)->nombre), 1, *archivo);
        fread(contacto->telefono, sizeof((*lista)->telefono), 1, *archivo);
        contacto->siguiente = NULL;
        if (*contacto->nombre != '\0' && *contacto->telefono != '\0') {
            agregar_contacto(lista, &contacto);
        }
    }
    fclose(*archivo);
}
