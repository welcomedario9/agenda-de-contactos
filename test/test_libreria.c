// test_libreria.c
//------------------------------------------------------------------------------
// Esta es una prueba de la librería <stdlist.h>
//------------------------------------------------------------------------------

// Librerías
#include <stdio.h>
#include "../lib/stdlist.h"


// Función principal
int main(int argc, char *argv[]) {
    // variables 
    int contador = 0;
    nodo *lista = NULL;
    nodo *contacto = NULL;
    FILE *lista_file;
    // bucle principal    
    /** while (contador < 5) { */
    /**     contacto = crear_contacto(); */
    /**     agregar_contacto(&lista, &contacto); */
    /**     contador++; */
    /** } */
    char nombre[20];
    printf("Ingrese un nombre: ");
    fgets(nombre, 20, stdin);
    removeln(nombre);
    // descargar la lista
    cargar_lista(&lista, &lista_file);
    contacto = seleccionar_contacto(&lista, nombre);
    printf("Contacto seleccionado: %s", contacto->nombre);
    imprimir_lista(lista);
    /** descargar_lista(&lista, &lista_file); */
    // fin del programa
    return 0;
}
