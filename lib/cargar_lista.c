// cargar_lista.c
//------------------------------------------------------------------------------
#include "stdlist.h"

// Esta función carga "lee" un archivo de disco duro en donde se encuentra
// almacenada la lista de contactos, los carga uno por uno en una lista enlazada
// para poder gestionar los datos luego y usarlos dentro de la aplicación.
void cargar_lista(nodo **lista, FILE **archivo) {
    // variables
    char *nombre_archivo = "lista_contactos.txt";
    char *modo_apertura = "rb";
    // inicialización del archivo
    *archivo = fopen(nombre_archivo, modo_apertura);

    // TODO: Hacer una comprobación de validar_archivo()
    validar_archivo(archivo);
    // contacto
    nodo *contacto = NULL; 
    // bucle
    while (!feof(*archivo)) {
        contacto = (nodo *) malloc(sizeof(nodo));

        // TODO: Hace una comprobación de validar_memoria()
        validar_memoria(&contacto);

        fread(contacto->nombre, sizeof((*lista)->nombre), 1, *archivo);
        fread(contacto->telefono, sizeof((*lista)->telefono), 1, *archivo);
        contacto->siguiente = NULL;
        if (*contacto->nombre != '\0' && *contacto->telefono != '\0') {
            agregar_contacto(lista, &contacto);
        }
    }
    fclose(*archivo);
}
