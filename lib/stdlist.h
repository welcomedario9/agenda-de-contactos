// stdlist.h
//------------------------------------------------------------------------------
// Este es el encabezado de la librería standard list, que contine todas las 
// estructuras, typedefs, enums, prototipos necesarios para la implemantación de
// listas enlazadas.
//------------------------------------------------------------------------------

#ifndef _STDLIST_H
#define _STDLIST_H


// Librerías
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Estructuras.
typedef struct nodo {
    char nombre[20];
    char telefono[15];
    struct nodo *siguiente;
} nodo;

// Prototipos de manejo de strings.
void removeln(char *string);

// Prototipos de validación.
const char *validar_lista(nodo **lista);
const char *validar_memoria(nodo **memoria);
const char *validar_archivo(FILE **archivo);

// Protitipos para el manejo de datos del contacto.
nodo *pedir_datos(nodo **contacto);
nodo *crear_contacto(void);
nodo *seleccionar_contacto(nodo **lista, const char *registro);

// Prototipos de manejo de contactos en la lista.
void agregar_contacto(nodo **lista, nodo **contacto);
void imprimir_lista(nodo *lista);

// Prototipos de carga y descaga de la lista hacia archivos externos.
void cargar_lista(nodo **lista, FILE **archivo);
void descargar_lista(nodo **lista, FILE **archivo);


//------------------------------------------------------------------------------
#endif  // _STDLIST_H
