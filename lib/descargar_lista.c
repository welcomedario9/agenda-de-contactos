// descargar_lista.c
//------------------------------------------------------------------------------
#include "stdlist.h"

// Esta función descarga "escribe" la lista en una archivo en el disco duro, 
// recibe un apuntador a la lista y un apuntador al archivo donde será almacenada
// la lista. 
void descargar_lista(nodo **lista, FILE **archivo) {

    // TODO: Hacer una comprobación de validar_lista()
    validar_lista(lista);

    char *nombre_archivo = "lista_contactos.txt";
    char *modo_apertura = "ab";
    *archivo = fopen(nombre_archivo, modo_apertura);

    // TODO: Hacer una comprobación de validar_archivo()
    validar_archivo(archivo);

    // procedimiento
    while (*lista) {
        fwrite((*lista)->nombre, sizeof((*lista)->nombre), 1, *archivo);
        fwrite((*lista)->telefono, sizeof((*lista)->telefono), 1, *archivo);
        *lista = (*lista)->siguiente;
    }
    fclose(*archivo);
}
