// seleccionar_contacto.c 
//------------------------------------------------------------------------------
#include "stdlist.h"

// Esta función recibe un apuntador bidimensional a la lista y un apuntador de tipo
// cadena a un registro, compara el registro en la lista y en caso de que el registro
// se haya encontrado dentro de la lista, devuelve un apuntador al contacto.
nodo *seleccionar_contacto(nodo **lista, const char *registro) {
    // variables
    nodo *auxiliar = NULL;
    // valida si la lista esta vacía
    if (validar_lista(lista)) {
        puts(validar_lista(lista));
        exit(1);
    } 
    else {
        while(*lista) {
            if (strcmp((*lista)->nombre, registro) == 0 || strcmp((*lista)->telefono, registro) == 0) {
                /** auxiliar = *lista; */
                return *lista;
            }
            *lista = (*lista)->siguiente;
        }
    }
    return auxiliar;
}
