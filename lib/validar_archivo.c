// validar_archivo.c 
#include "stdlist.h"

// Esta función valída que el archivo haya sido abierto correctamente, en caso 
// contrario devuelve un mensaje de error.
const char *validar_archivo (FILE **archivo) {
    if (*archivo == NULL) return "¡Error! El archivo no se abrió correctamente.";
    return "";
}
