// pedir_datos.c
#include "stdlist.h"

// Esta función recibe un apuntador bidimensional al contacto, luego solicita los 
// datos del contacto al usuario los agrega al contacto, luego devuelve un apuntador
// a ese contacto.
nodo *pedir_datos(nodo **contacto) {
    printf("Digite el nombre del contacto: ");
    fgets((*contacto)->nombre, 20, stdin);
    removeln((*contacto)->nombre);
    printf("Digite el telefono del contacto: ");
    fgets((*contacto)->telefono, 15, stdin);
    removeln((*contacto)->telefono);
    (*contacto)->siguiente = NULL;
    return *contacto;
}
