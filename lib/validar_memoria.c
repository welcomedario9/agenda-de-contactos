// validar_memoria.c
//------------------------------------------------------------------------------
#include "stdlist.h"

// Esta función valída que la memoria haya sido reservada correctamente, en caso
// contrario devuelve un mensaje de error y finaliza la aplicación.
const char *validar_memoria(nodo **memoria) {
    if (*memoria == NULL) return "¡Error! La memoria no se reservó correctamente.";
    return "";
}
