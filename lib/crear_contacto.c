// crear_contacto.c
#include "stdlist.h"

// Esta función no recibe nada, crea un apuntador a contacto asigna los datos a ese
// contacto y luego devuelve un apuntador a dicho contacto.
nodo *crear_contacto(void) {
    // reserva espacio en memoria para un nuevo nodo. 
    nodo *contacto = (nodo *) malloc(sizeof(nodo));

    // valída si se reservó la memoria correctamente.
    if (validar_memoria(&contacto)) {
        puts(validar_memoria(&contacto));
        exit(1);
    }

    // pedir datos
    contacto = pedir_datos(&contacto);
    
    // devuelve el apuntador al contacto
    return contacto;
}
