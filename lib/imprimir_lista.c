// imprimir_lista.c
//------------------------------------------------------------------------------
#include "stdlist.h"

// Recibe un apuntador a la lista y la despliega en pantalla.
void imprimir_lista(nodo *lista) {
    // valída si la lista está vacía
    if (!lista) puts("¡La lista está vacía!");
    else {
        puts("============================================================");
        while (lista) {
            printf("Contacto: %-15s\t", lista->nombre);
            printf("Telefono: %s\n", lista->telefono);
            lista = lista->siguiente;
        }
    }
}
