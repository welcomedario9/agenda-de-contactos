// agregar_contacto.c
//------------------------------------------------------------------------------
#include "stdlist.h"

// Esta función agrega un contacto determinado a una lista determinada.
void agregar_contacto(nodo **lista, nodo **contacto) {
    // valída si la lista está vacía.
    if (*lista == NULL) *lista = *contacto;
    else {
        // se necesita utilizar un nodo auxiliar para que la lista no se sobre
        // escriba asi misma con solo los ultimos dos valores.
        nodo *auxiliar = *lista;
        // si hay más valores, entonces adelanta el apuntador en el ciclo anterior 
        // hasta la ultima posición para que el nuevo valor sea agregado en el
        // siguiente de la ultima posición encontrada.
        while (auxiliar->siguiente) {
            auxiliar = auxiliar->siguiente;
        }
        // si solo hay un valor en la lista, el segundo valor se agrega en siguiente.
        auxiliar->siguiente = *contacto;
    }
}
