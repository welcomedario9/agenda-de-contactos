// validar_lista.c
//------------------------------------------------------------------------------
#include "stdlist.h"

// Esta función valída que la lista no este vacía. Si la lista está vacía 
// devuelve un apuntador al mensaje.
const char *validar_lista(nodo **lista) {
    if (*lista == NULL) return "¡Error, La lista está vacía!";
    return "";
}
