### Manejo de datos

1. *Pedir datos del contacto:*
    + Pedir al usuario el nombre del contacto
    + Almacenar nombre en una variable de contacto
    + Pedir al usuario el número telefónico del contacto
    + Almacenar el número telefónico en una variable de contacto

```
procedimiento pedir_contacto(contacto: Contacto)
variables
    caracter: nombre[25]
    caracter: telefono[15]
inicio
    escribir "Ingrese el nombre del contacto:"
    leer(nombre)
    escribir "Ingrese el telefono del contacto:"
    leer(telefono)
    contacto.nombre = nombre
    contacto.telefono = telefono
fin
```
