### Manejo de archivos

1. *Obtener lista del archivo:*
    + Abrir el archivo del disco
    + Leer contacto del archivo y cargarlo en la lista
    + Apuntar la lista al siguiente contacto
    + Repetir los dos anteriores mientras no haya llegado al final del archivo
    + Cerrar el archivo de disco

```
procedimiento obtener_lista(lista: Lista)
variables
    caracter: nombre_archivo[] = "lista_contactos.txt"
    caracter: modo_apertura[] = "rb+"
    Archivo: archivo = abrir(nombre_archivo, modo_apertura)
    Nodo: apuntador
inicio
    apuntador = lista.cabeza
    si (archivo == NULL) 
        escribir "Error abriendo el archivo"
        salida (1)
    fin si 
    mientras (!feof)
        leer archivo(apuntador.contacto, tamaño(contacto), 1, archivo)
        apuntador = apuntador.siguiente
    fin mientras
    cerrar(archivo)
fin 
```

2. *Guardar lista actualizada:*
    + Abrir el archivo de disco
    + Escribir contacto de la lista en el archvivo
    + Apuntar la lista al siguiente contacto
    + Repetir los dos anteriores mientras no haya llegado al final de la lista
    + Cerrar el archvivo de disco

```
procedimiento guardar_lista(lista: Lista)
variables
    caracter: nombre_archivo[] = "lista_contactos.txt"
    caracter: modo_apertura[] = "rb+"
    Archivo: archivo = abrir(nombre_archivo, modo_apertura)
    Nodo: apuntador
inicio 
    apuntador = lista.cabeza
    si (archivo == NULL)
        escribir "Error abriendo el archivo"
        salida (1)
    fin si
    mientras (apuntador.siguiente != NULL)
        escribir archivo(apuntador.contacto, tamaño(contacto), 1, archivo)
        apuntador = apuntador.siguiente
    fin mientras
    cerrar(archivo)
fin
```
