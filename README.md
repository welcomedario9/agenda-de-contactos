# Agenda de Contactos

Este proyecto es la aplicación de todos los conocimientos adquiridos en Lenguaje
C, para desarrollar una *Agenda de contactos telefónicos* que se guardarán en un
archivo en el disco duro.

Esta aplicación hace uso del paradigma de programación estructurada, apuntadores
, estructuras de datos "listas enlazadas", lectura y escritura de archivos.

## Tareas principales 

Esta aplicación se divide en las siguientes tareas principales:

1. Agregar Contacto
2. Actualizar Contacto
3. Eliminar Contacto
4. Mostrar Contacto
5. Lista de Contactos

### Subtareas

Cada tarea principal se puede sudividir en tareas mas pequeñas, a fín de ahorrar
tiempo, encerrando en funciones y procedimientos las tareas repetitivas que sean
utilizadas en varias partes de la aplicación.

1. Agregar Contacto
    + Cargar la lista desde un archivo 
    + Crear contacto a partir de datos ingresados
    + Agregar contacto
    + Ordenar lista de contactos 
    + Descargar la lista hacia un archivo 

2. Actualizar Contacto
    + Cargar la lista desde un archivo
    + Seleccionar contacto en la lista 
    + Crear contacto a partir de datos ingresados
    + Actualizar contacto
    + Ordenar lista de contactos
    + Descargar la lista hacia un archivo 

3. Eliminar Contacto
    + Cargar la lista desde un archivo
    + Seleccionar contacto en la lista de la lista
    + Eliminar contacto de la lista
    + Ordenar lista de contactos
    + Descargar la lista hacia un archivo 

4. Mostrar Contacto
    + Cargar la lista desde un archivo
    + Seleccionar contacto en la lista 
    + Imprimir datos del contacto

5. Lista de Contactos
    + Cargar la lista desde un archivo
    + Imprimir lista de contactos 

Las subtareas que se repiten entre las tareas principales, podemos agruparlas en
funciones o procedimientos para poder reutilizar el código en las tareas que lo
ocupemos. A continuación se enumerarán las subtareas que se repiten:

#### Categorías

1. Gestión de archivos de disco
    + Cargar la lista desde un archivo
    + Descargar la lista hacia un archivo

2. Gestión de datos del contacto
    + Crear contacto a partir de datos ingresados
    + Seleccionar datos del contacto en la lista 

3. Administración de la lista
    + Agregar contacto 
    + Actualizar contacto
    + Eliminar contacto
    + Imprimir datos del contacto
    + Ordenar lista de contactos
    + Imprimir lista de contactos

##### Gestión de archivos de disco

###### Cargar la lista desde un archivo

Este algoritmo carga "lee" un archivo de disco duro en donde se encuentra
almacenada la lista de contactos, los carga uno por uno en una lista enlazada
para poder gestionar los datos luego y usarlos dentro de la aplicación.

**Algoritmo:** 
==============
    + Abrir el archivo del disco
    + Leer contacto del archivo y cargarlo en la lista
    + Apuntar la lista al siguiente contacto
    + Repetir los dos anteriores mientras no haya llegado al final del archivo
    + Cerrar el archivo de disco

**Pseudocódigo:** 

```
procedimiento cargar_lista(lista: Nodo)
variables
    caracter: nombre_archivo[] = "lista_contactos.txt"
    caracter: modo_apertura[] = "rb+"
    Archivo: archivo = abrir_archivo(nombre_archivo, modo_apertura)
inicio
    si (archivo == NULL) 
        escribir "Error abriendo el archivo"
        salida (1)
    fin si 
    nodo: *contacto; 
    mientras (!feof(archivo))
        contacto = (nodo *) malloc(sizeof(nodo))
        validar_memoria(contacto)
        leer_archivo(contacto, tamaño(contacto), 1, archivo)
        contacto.siguiente = NULL
        si (contacto.nombre != '\0' && contacto.telefono != '\0')
            agregar_contacto(lista, contacto)
        fin si
    fin mientras
    cerrar(archivo)
fin procedimiento
```

######  Descargar la lista hacia un archivo

Este algoritmo descarga "guarda" los datos de la lista uno por uno en un archivo
de disco externo, esto nos permite acceder posteriormente a los datos ingresados
entre cada uso de la aplicación.

**Algoritmo:** 

    + Abrir el archivo de disco
    + Escribir contacto de la lista en el archvivo
    + Apuntar la lista al siguiente contacto
    + Repetir los dos anteriores mientras no haya llegado al final de la lista
    + Cerrar el archvivo de disco

**Pseudocódigo:**

```
procedimiento decargar_lista(lista: Nodo)
variables
    caracter: nombre_archivo[] = "lista_contactos.txt"
    caracter: modo_apertura[] = "wb+"
    Archivo: *archivo = abrir_archivo(nombre_archivo, modo_apertura)
inicio 
    si (lista != NULL)
        si (archivo == NULL)
            escribir "Error abriendo el archivo"
            salida (1)
        sino
            mientras (lista)
                escribir_archivo(lista.contacto, tamaño(contacto), 1, archivo)
                lista = lista.siguiente
            fin mientras
            cerrar_archivo(archivo)
        fin si
    sino
        escribir "¡La lista está vacía!"
        salida (1)
    fin si
fin procedimiento
```

##### Gestión de datos del contacto

###### Crear contacto a partir de datos ingresados

Este algoritmo algoritmo solicita al usuario que ingrese por teclado los datos 
de un contacto: nombre y número telefónico. Luego crea un contacto a partir de 
esos datos, el cual podrá ser agregado a la lista en la aplicación.

**Algoritmo:**

    + Crea un apuntador de tipo nodo llamado contacto
    + Reserva memoria para el nodo contacto
    + Valída que la memoria se haya reservado correctamente
    + Solicita que se ingrese el nombre del contacto
    + Almacena el nombre en el nodo contacto en el espacio nombre
    + Solicita que se ingrese el número telefónico del contacto
    + Almacena el telefono en el nodo contacto en el espacio telefono
    + Apunta el nodo contacto siguiente a nulo
    + Retorna el nodo contacto

**Pseudocódigo: Crear contacto a partir de datos ingresados**

```
función crear_contacto(): nodo
variables
    nodo: *contacto
inicio
    contacto = (nodo *) malloc(sizeof(nodo))
    validar_memoria(contacto)
    escribir "Nombre: "
    leer (contacto->nombre)
    escribir "Telefono"
    leer (contacto->telefono)
    contacto->siguiente = NULL
    devolver: contacto
fin función    
```

###### Seleccionar datos del contacto en la lista

Este algoritmo solicita al usuario que ingrese el nombre de un contacto y lo 
busca en la lista, al tener ubicado el contacto, declara un contacto auxiliar 
y lo apunta hacia el contacto deseado, luego devuelve el contacto auxiliar.

**Algoritmo:**
=================
    + Pedir al usuario el nombre o telefono del contacto a buscar
    + buscar dentro de la lista el nombre o telefono del contacto
    + Apuntar un nodo auxiliar a ese contacto en la lista
    + Retornar el nodo auxiliar.

**Pseudocódigo:**

```
función Seleccionar_contacto(lista: nodo, registro: const char): nodo
variables
    caracter: registro[20]
    nodo: *auxiliar
inicio
    si (validar_lista(lista))
        escribir validar_lista(lista)
        break
    sino
        mientras (lista) 
            si (strcmp(lista->nombre, registro) == 0 || strcmp(lista->telefono, registro)) 
                auxiliar = lista
                devolver: auxiliar
            fin si
            lista = lista->siguiente
        fin mientras
    fin si
    devolver: auxiliar
fin función       
```
